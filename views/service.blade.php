<div class="card">
    <div class="card-header" id="heading{{ $service->id }}">
        <h5 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#service_{{ $service->id }}" aria-expanded="true" aria-controls="collapse{{ $service->id }}">
                {{ $service->name }}
            </button>
        </h5>
    </div>

    <div id="service_{{ $service->id }}" class="collapse" aria-labelledby="heading{{ $service->id }}" data-parent="#mainAccordion">
        <div class="card-body" id="details_{{ $service->id }}">
        </div>
    </div>
</div>