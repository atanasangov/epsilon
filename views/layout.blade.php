<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <title>{{ $title }}</title>

        <script lang="text/javascript">
            $(function(){
                $('.collapse').on('show.bs.collapse', function () {
                    let serviceId = $(this).attr('id').split('_')[1]
                    $("#details_" + serviceId).html("Fetching details for service: " + serviceId)
                    loadDetails(serviceId)
                })
            })

            function loadDetails(serviceId){
                $.ajax({
                    type: "POST",
                    url: '/get_service_details.php',
                    data: {"serviceId": serviceId},
                    dataType: 'json'
                }).done(function(data){
                    console.log(data)
                    $("#details_" + serviceId).html("Service ID: " + data.id + ", Name: " + data.name + ", Status: " + data.status);
                }).fail(function(jqXHR, textStatus, errorThrown){
                    console.log(errorThrown)
                })
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="accordion" id="mainAccordion">

                @foreach ($services as $service)
                    @include('service')
                @endforeach

            </div>
        </div>
    </body>
</html>