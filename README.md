# Prerequisites
-
You should have vagrant installed.
   
# Installation instructions

1. Checkout the code: 
    ````
    git clone https://atanasangov@bitbucket.org/atanasangov/epsilon.git
    ````
2. Navigate to the directory and run
    ````
    composer install
    ````
3. Edit the Vagrantfile line: 
    ````
    config.vm.network "public_network", ip: "192.168.10.197"
    ```` 
    to reflect to your network settings, i.e. change the IP to one of your network's range. 

4. Add an entry in your OS' hosts file. Example:
    ````
    192.168.10.197			dev.epsilon.angov.org
    ````

5. Execute ````vagrant up```` 
    
6. Fire up a browser and hit [http://dev.epsilon.angov.org](http://dev.epsilon.angov.org)

7. (Optional) You can run the tests by issuing: 
      ````
      php library/phpunit/phpunit/phpunit --configuration phpunit.xml core
      ```` 