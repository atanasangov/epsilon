<?php
require_once 'autoload.php';
require('../library/autoload.php');
$serviceId = (int)$_POST['serviceId'];

$client = new \core\UnirestEpsilonClient();
$serviceDetails = $client->getServiceDetails($serviceId);
echo json_encode($serviceDetails);