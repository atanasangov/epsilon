<?php
error_reporting(E_ALL);
ini_set('memory_limit','512M');
ini_set('display_errors', 'On');
require_once 'autoload.php';
require('../library/autoload.php');

$client = new \core\UnirestEpsilonClient();
$services = $client->getServices();

$blade = new \Jenssegers\Blade\Blade(['../views'], '../views/cache');

echo $blade->render('layout', ['services' => $services, 'title' => 'Atanas TechTest']);