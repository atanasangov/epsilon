<?php

namespace core;


use GuzzleHttp\Client;

class GuzzleEpsilonClient implements HttpClient
{
    private const CLIENT_ID = 'SrFoh9fjxOwnkiwCXqw4susUnWEV4Yf3djiKU2d4';
    private const CLIENT_SECRET = 'cyvNtmJm76pfl9l3E35TRoKhAIhFPxdAVgw0NddL';
    private const BASE_URI = 'http://demo.infiny.cloud/api/';
    private const API_ACCESS_TOKEN = 'oauth2/access-token';
    private const API_SERVICES = 'services';
    private const API_SERVICE_DETAILS = 'services/%d/service';
    private const API_ACCEPT_HEADER = 'application/vnd.cloudlx.v1+json';
    private const API_AUTHORIZATION_HEADER = 'Bearer %s';
    protected $httpClient;


    public function __construct($httpClient = null)
    {
        $this->httpClient = $httpClient ?? new Client();
    }

    public function getServices(): array
    {
        $response = $this->httpClient->request('GET', self::BASE_URI . self::API_SERVICES, [
            'headers' => ['Accept' => self::API_ACCEPT_HEADER, 'Authorization' => sprintf(self::API_AUTHORIZATION_HEADER, $this->getAccessToken())],
        ]);

        $decoded = \GuzzleHttp\json_decode($response);
        return $decoded->services;
    }

    public function getServiceDetails(int $serviceId): object
    {
        $response = $this->httpClient->request('GET', self::BASE_URI . sprintf(self::API_SERVICE_DETAILS, $serviceId), [
            'headers' => ['Accept' => self::API_ACCEPT_HEADER, 'Authorization' => sprintf(self::API_AUTHORIZATION_HEADER, $this->getAccessToken())],
        ]);
        $decoded = \GuzzleHttp\json_decode($response);
        return $decoded;
    }

    public function getAccessToken()
    {
        $response = $this->httpClient->request('POST', self::BASE_URI . self::API_ACCESS_TOKEN, [
            'headers' => [
                'Accept' => self::API_ACCEPT_HEADER,
            ],
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => self::CLIENT_ID,
                'client_secret' => self::CLIENT_SECRET,
            ],
            //'debug' => true
        ]);

        // echo $response->getBody();
        $decoded = \GuzzleHttp\json_decode($response);
        return $decoded->access_token;
    }
}