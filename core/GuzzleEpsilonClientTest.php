<?php

use core\GuzzleEpsilonClient;

class GuzzleEpsilonClientTest extends PHPUnit_Framework_TestCase
{
    public function testGetServices()
    {
        $guzzleHttpClientMock = $this->getGuzzleMock();

        $guzzleHttpClientMock->expects($this->exactly(2))
            ->method('request')
            ->withConsecutive(
                [ // First batch of arguments
                    'POST',
                    'http://demo.infiny.cloud/api/oauth2/access-token',
                    [
                        'headers' => [
                            'Accept' => 'application/vnd.cloudlx.v1+json'
                        ],
                        'form_params' => [
                            'grant_type' => 'client_credentials',
                            'client_id' => 'SrFoh9fjxOwnkiwCXqw4susUnWEV4Yf3djiKU2d4',
                            'client_secret' => 'cyvNtmJm76pfl9l3E35TRoKhAIhFPxdAVgw0NddL',
                        ]
                    ]
                ],
                [ // Second batch of arguments
                    'GET',
                    'http://demo.infiny.cloud/api/services',
                    [
                        'headers' => [
                            'Accept' => 'application/vnd.cloudlx.v1+json',
                            'Authorization' => 'Bearer another_random_token'
                        ]
                    ]
                ]
            )
            ->willReturnOnConsecutiveCalls(
                '{"access_token": "another_random_token","token_type": "Bearer","expires_in": 3600, "refresh_token": "a_refresh_token"}',
                '{"services": [
                            {"id": 196, "name": "Azure Test", "vlan": 101 },
                            {"id": 197, "name": "AWS Test", "vlan": 131 },
                            {"id": 198, "name": "Epsilon Test", "vlan": 101 }
                        ]
                }'
            );

        $epsilonClient = new GuzzleEpsilonClient($guzzleHttpClientMock);
        $services = $epsilonClient->getServices();
        $this->assertCount(3, $services);
        $this->assertSame('AWS Test', $services[1]->name);
    }

    public function testGetServiceDetails()
    {
        $guzzleHttpClientMock = $this->getGuzzleMock();

        $guzzleHttpClientMock->expects($this->exactly(2))
            ->method('request')
            ->withConsecutive(
                [ // First batch of arguments (for getting the access token)
                    'POST',
                    'http://demo.infiny.cloud/api/oauth2/access-token',
                    [
                        'headers' => [
                            'Accept' => 'application/vnd.cloudlx.v1+json'
                        ],
                        'form_params' => [
                            'grant_type' => 'client_credentials',
                            'client_id' => 'SrFoh9fjxOwnkiwCXqw4susUnWEV4Yf3djiKU2d4',
                            'client_secret' => 'cyvNtmJm76pfl9l3E35TRoKhAIhFPxdAVgw0NddL',
                        ]
                    ]
                ],
                [ // Second batch of arguments
                    'GET',
                    'http://demo.infiny.cloud/api/services/38/service',
                    [
                        'headers' => [
                            'Accept' => 'application/vnd.cloudlx.v1+json',
                            'Authorization' => 'Bearer another_random_token'
                        ]
                    ]
                ]
            )
            ->willReturnOnConsecutiveCalls(
                '{"access_token": "another_random_token","token_type": "Bearer","expires_in": 3600, "refresh_token": "a_refresh_token"}',
                '{
                    "id": 10,
                    "name": "Azure test",
                    "vlan": 101,
                    "nni_vlan": 3600,
                    "created": "2015-12-07 14:07:54",
                    "expires": "2015-12-08 14:07:49",
                    "bandwidth": "200Mbps",
                    "paused": false,
                    "expired": false,
                    "type": "Microsoft Azure",
                    "status": "up",
                    "port": {
                        "id": 219,
                        "name": "LRHosting NYC Port 1",
                        "index": 18,
                        "switch": {
                            "id": 1,
                            "index": 1
                        },
                        "slot": {
                            "id": 1,
                            "index": 1
                        },
                        "shelf": {
                            "id": 1,
                            "index": 1
                        }
                    }
                }'
            );

        $epsilonClient = new GuzzleEpsilonClient($guzzleHttpClientMock);
        $serviceDetails = $epsilonClient->getServiceDetails(38);
        $this->assertSame('Azure test', $serviceDetails->name);
        $this->assertSame('LRHosting NYC Port 1', $serviceDetails->port->name);
    }

    private function getGuzzleMock()
    {
        return $this->getMockBuilder(GuzzleHttp\Client::class)
            ->disableOriginalConstructor()
            ->setMethods(['request'])
            ->getMock();
    }
}