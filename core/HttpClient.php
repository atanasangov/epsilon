<?php

namespace core;


interface HttpClient
{
    public function getServices(): array;
    public function getServiceDetails(int $serviceId): object;
}