<?php

namespace core;


use Unirest\Request;

class UnirestEpsilonClient implements HttpClient
{
    private const CLIENT_ID = 'SrFoh9fjxOwnkiwCXqw4susUnWEV4Yf3djiKU2d4';
    private const CLIENT_SECRET = 'cyvNtmJm76pfl9l3E35TRoKhAIhFPxdAVgw0NddL';
    private const BASE_URI = 'http://demo.infiny.cloud/api/';
    private const API_ACCESS_TOKEN = 'oauth2/access-token';
    private const API_SERVICES = 'services';
    private const API_SERVICE_DETAILS = 'services/%d/service';
    private const API_ACCEPT_HEADER = 'application/vnd.cloudlx.v1+json';
    private const API_AUTHORIZATION_HEADER = 'Bearer %s';


    public function getServices(): array
    {
        $response = Request::get($this->getServiceEndpointUri(), $this->getExtendedHeaders());
        return $response->body->services;
    }

    public function getServiceDetails(int $serviceId): object
    {
        $response = Request::get($this->getServiceDetailsEndpointUri($serviceId), $this->getExtendedHeaders());
        return $response->body;
    }

    private function getAccessToken(): string
    {
        $headers = $this->getCommonHeaders();
        $params = [
            'grant_type' => 'client_credentials',
            'client_id' => self::CLIENT_ID,
            'client_secret' => self::CLIENT_SECRET
        ];

        $response = Request::post($this->getAuthorizationEndpointUri(), $headers, $params);

        return $response->body->access_token;
    }

    private function getCommonHeaders(): array
    {
        return [
            'Accept' => self::API_ACCEPT_HEADER
        ];
    }

    private function getExtendedHeaders(): array
    {
        $extended = $this->getCommonHeaders();
        $extended['Authorization'] = sprintf(self::API_AUTHORIZATION_HEADER, $this->getAccessToken());
        return $extended;
    }

    private function getServiceDetailsEndpointUri(int $serviceId): string
    {
        return self::BASE_URI . sprintf(self::API_SERVICE_DETAILS, $serviceId);
    }

    private function getServiceEndpointUri(): string
    {
        return self::BASE_URI . self::API_SERVICES;
    }

    private function getAuthorizationEndpointUri(): string
    {
        return self::BASE_URI . self::API_ACCESS_TOKEN;
    }
}