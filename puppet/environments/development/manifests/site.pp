exec { 'apt-get install software-properties-common':
  command => "/usr/bin/apt-get -y install software-properties-common"
}

exec { 'add-ondrej-ppa':
  command => "/usr/bin/sudo /usr/bin/add-apt-repository ppa:ondrej/php"
}

exec { 'apt-get update':
  command => "/usr/bin/apt-get update"
  #onlyif => "/bin/bash -c 'exit $(( $(( $(date +%s) - $(stat -c %Y /var/lib/apt/lists/$( ls /var/lib/apt/lists/ -tr1|tail -1 )) )) <= 604800 ))'"
}

package { 'git':
  ensure => present;
}

package {'zip':
  ensure => present;
}

package {'unzip':
  ensure => present;
}

# php-pear package
package { ["php-pear"] :
  ensure => "installed"
}

package { "vim" :
  ensure => present;
}

# set channels to auto discover
exec { "pear auto_discover" :
  command => "/usr/bin/pear config-set auto_discover 1",
  require => [Package['php-pear']]
}

exec { "pear update-channels" :
  command => "/usr/bin/pear update-channels",
  require => [Package['php-pear']]
}

# pear upgrade
exec { "pearUpgrade":
  command => "/usr/bin/pear upgrade --force",
  require => Package["php-pear"]
}

# phing
exec { "phing":
  command => "/usr/bin/pear channel-discover pear.phing.info; /usr/bin/pear install phing/phing; /usr/bin/pear install HTTP_Request2",
  unless => "/usr/bin/pear info phing/phing",
  require => Exec["pearUpgrade"]
}

exec { "versioncontrol_git":
  command => "/usr/bin/pear install VersionControl_Git-0.4.4",
  unless => "/usr/bin/pear info versioncontrol_git"
}

include php, apache, composer::install
