# Install composer

class composer::install {

  package { "curl":
    ensure => installed,
  }

  exec { 'composer-install':
    command => 'wget --quiet https://getcomposer.org/composer.phar -O composer.phar && chmod +x composer.phar && sudo mv composer.phar /usr/local/bin/composer',
    path    => '/usr/bin:/usr/sbin:/bin'
  }

}