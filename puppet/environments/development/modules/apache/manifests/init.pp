class apache{
  package { ['apache2']:
    ensure => present;
  }

  service { 'apache2':
    ensure  => running,
    require => Package['apache2'];
  }

  file { "/etc/apache2/mods-enabled/rewrite.load":
    ensure  => link,
    target  => "/etc/apache2/mods-available/rewrite.load",
    require => Package['apache2'],
    notify  => Service['apache2'];
  }

  file { '/var/www':
    ensure => directory;
  }

  file { 'vagrant-vhost.conf':
    path    => "/etc/apache2/sites-available/vagrant-vhost.conf",
    source  => "puppet:///modules/apache/vagrant-vhost.conf",
    require => Package['apache2'],
    notify  => Service['apache2'],
    owner   => 'root',
    mode    => '0644',
  }

  file { '000-default.conf':
    path    => "/etc/apache2/sites-enabled/000-default.conf",
    require => Package['apache2'],
    notify  => Service['apache2'],
    ensure  => absent,
  }

  file { '/etc/apache2/sites-enabled/vagrant-vhost.conf':
    ensure => link,
    target => "/etc/apache2/sites-available/vagrant-vhost.conf",
    require => File['vagrant-vhost.conf'],
    notify => Service['apache2'];
  }

  # Disable apache's mpm_event module
  exec { "disable mpm_event":
    command => "/usr/sbin/a2dismod mpm_event"
  }
  # and enable mpm_prefork
  exec { "enable prefork":
    command => "/usr/sbin/a2enmod mpm_prefork"
  }
  # and enable php
  exec { "enable php7":
    command => "/usr/sbin/a2enmod php7.3"
  }
  # and restart apache
  exec { "restart apache":
    command => "/usr/sbin/service apache2 restart"
  }
}