# Installs php, php modules and config files
class php {
  package { [
    'php7.3',
    'php7.3-mysql',
    'php7.3-zip',
    'libapache2-mod-php7.3',
    'php-xdebug',
    'php-gd',
    'php7.3-xml',
    'php7.3-curl',
    'php7.3-mbstring'
  ]:
    ensure => present,
    notify  => Service['apache2'],
  }

  # file {  '/etc/php5/apache2':
  #   ensure => directory,
  #   before => File['/etc/php5/apache2/php.ini'];
  # }

  # file {  '/etc/php5/apache2/php.ini':
  #   source  => 'puppet:///modules/php/php.ini',
  #   require => Package['php5'];
  # }
}